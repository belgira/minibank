package com.intevalue.minibank.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.intevalue.minibank.entity.Users;

@Repository
public interface UsersRepository extends CrudRepository<Users, Integer> {

	public Users findByUserId(String userId);
	
}
