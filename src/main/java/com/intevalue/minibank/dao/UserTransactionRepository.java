package com.intevalue.minibank.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.intevalue.minibank.entity.UserTransaction;

@Repository
public interface UserTransactionRepository extends CrudRepository<UserTransaction, Integer> {
	
	public List<UserTransaction> findByUserId(String userId);

}
