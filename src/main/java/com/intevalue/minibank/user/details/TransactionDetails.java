package com.intevalue.minibank.user.details;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intevalue.minibank.dao.UserTransactionRepository;
import com.intevalue.minibank.entity.UserTransaction;

@Component
public class TransactionDetails {
	
	@Autowired
	private UserTransactionRepository userTransactionRepository;
	
	public String getTransactions(String userId) {
		List<UserTransaction> transactionList = userTransactionRepository.findByUserId(userId);		
		JSONObject json = new JSONObject();
		for (UserTransaction userTransaction : transactionList) {
			JSONObject transaction = new JSONObject();
			transaction.put("userId", userTransaction.getUserId());
			transaction.put("message", userTransaction.getMessage());
			transaction.put("transactionDate", userTransaction.getTransactionDate());
			json.append("transactions", transaction);
		}
		return json.toString();
	}


}
