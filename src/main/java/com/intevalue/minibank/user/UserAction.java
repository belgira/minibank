package com.intevalue.minibank.user;

import java.math.BigDecimal;
import java.util.Date;
import java.util.stream.StreamSupport;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intevalue.minibank.dao.UserTransactionRepository;
import com.intevalue.minibank.dao.UsersRepository;
import com.intevalue.minibank.entity.UserTransaction;
import com.intevalue.minibank.entity.Users;

@Component
public class UserAction {
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private UserTransactionRepository userTransactionRepository;
	
	private final static String USER_ID = "userId";
	private final static String CURRENT_BALANCE = "currentBalance";
	
	public String getUserCount() {
		JSONObject json = new JSONObject();		
		Iterable<Users> users = usersRepository.findAll();		
		json.put("userCount", StreamSupport.stream(users.spliterator(), false).count());		
		for (Users user : users) {
			JSONObject userJson = new JSONObject();
			userJson.put(USER_ID, user.getUserId());
			userJson.put("firstName", user.getFirstName());
			userJson.put("lastName", user.getLastName());
			userJson.put(CURRENT_BALANCE, user.getCurrentBalance());
			json.append("users", userJson);
		}
		return json.toString();
	}
	
	public String deposit(String userId, String amount) {
		JSONObject json = new JSONObject();		
		Users user = usersRepository.findByUserId(userId);
		BigDecimal balance = user.getCurrentBalance();
		user.setCurrentBalance(new BigDecimal(amount).add(user.getCurrentBalance()));
		user = usersRepository.save(user);
		boolean isDepositSuccessful = false;
		if(user.getCurrentBalance().compareTo(balance) == 1) {
			isDepositSuccessful = true;
			UserTransaction userTransaction = new UserTransaction();			
			userTransaction.setMessage("Deposit amount: " + amount);
			userTransaction.setTransactionDate(new Date());
			userTransaction.setUserId(userId);
			userTransactionRepository.save(userTransaction);
		}
		
		json.put("isDepositSuccessful", isDepositSuccessful);
		json.put(CURRENT_BALANCE, user.getCurrentBalance());
		return json.toString();
	}
	
	public String withdrew(String userId, String amount) {
		JSONObject json = new JSONObject();		
		Users user = usersRepository.findByUserId(userId);
		BigDecimal balance = user.getCurrentBalance();
		// if balance is less than amount
		// dont allow withdraw
		boolean isWithdrawSuccessful = false;
		BigDecimal withdrawBalance = balance.subtract(new BigDecimal(amount));	
		if (withdrawBalance.compareTo(new BigDecimal("0")) == -1) {
			isWithdrawSuccessful = false;
			UserTransaction userTransaction = new UserTransaction();			
			userTransaction.setMessage("Insufficient balance");
			userTransaction.setTransactionDate(new Date());
			userTransaction.setUserId(userId);
			userTransactionRepository.save(userTransaction);
		} else {
			isWithdrawSuccessful = true;
			
			user.setCurrentBalance(withdrawBalance);
			user = usersRepository.save(user);	
			
			UserTransaction userTransaction = new UserTransaction();			
			userTransaction.setMessage("Withdraw amount: " + amount);
			userTransaction.setTransactionDate(new Date());
			userTransaction.setUserId(userId);
			userTransactionRepository.save(userTransaction);
		}
	
		json.put("isWithdrawSuccessful", isWithdrawSuccessful);
		json.put(CURRENT_BALANCE, user.getCurrentBalance());
		return json.toString();
	}

	public String transfer(String userId, String amount, String anotherUserId) {
		JSONObject json = new JSONObject();		
		Users user = usersRepository.findByUserId(userId);
		Users anotherUser = usersRepository.findByUserId(anotherUserId);
		BigDecimal balance = user.getCurrentBalance();
		// if balance is less than amount
		// dont allow withdraw
		boolean isTransferSuccessful = false;
		BigDecimal transferBalance = balance.subtract(new BigDecimal(amount));	
		if (transferBalance.compareTo(new BigDecimal("0")) == -1) {
			isTransferSuccessful = false;
			UserTransaction userTransaction = new UserTransaction();			
			userTransaction.setMessage("Insufficient balance");
			userTransaction.setTransactionDate(new Date());
			userTransaction.setUserId(userId);
			userTransactionRepository.save(userTransaction);
		} else {
			isTransferSuccessful = true;
			
			user.setCurrentBalance(transferBalance);
			user = usersRepository.save(user);				
			
			UserTransaction userTransaction = new UserTransaction();			
			userTransaction.setMessage("Transfer amount: " + amount);
			userTransaction.setTransactionDate(new Date());
			userTransaction.setUserId(userId);
			userTransactionRepository.save(userTransaction);
			
			anotherUser.setCurrentBalance(anotherUser.getCurrentBalance().add(new BigDecimal(amount)));
			anotherUser = usersRepository.save(user);	
									
			UserTransaction anotherUserTransaction = new UserTransaction();			
			anotherUserTransaction.setMessage("Received amount: " + amount);
			anotherUserTransaction.setTransactionDate(new Date());
			anotherUserTransaction.setUserId(anotherUserId);
			userTransactionRepository.save(anotherUserTransaction);
						
		}
	
		json.put("isTransferSuccessful", isTransferSuccessful);
		json.put(CURRENT_BALANCE, user.getCurrentBalance());
		return json.toString();
	}
	
	public String getBalance(String userId) {
		Users user = usersRepository.findByUserId(userId);		
		JSONObject json = new JSONObject();
		json.put(USER_ID, userId);
		json.put(CURRENT_BALANCE, user.getCurrentBalance());		
		return json.toString();
	}


}
