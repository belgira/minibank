package com.intevalue.minibank.service;

public abstract class BaseService {
	
	public abstract String getUserCount();
	
	public abstract String deposit(String userId, String amount);
	
	public abstract String withdrew(String userId, String amount);
	
	public abstract String transfer(String userId, String amount, String anotherUserId);
	
	public abstract String getTransactions(String userId);
		
	public abstract String getBalance(String userId);

}
