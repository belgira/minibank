package com.intevalue.minibank.service.impl;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.intevalue.minibank.service.BaseService;

@Service
public class Service0 extends BaseService {

	@Override
	public String getUserCount() {
		return "1000";
	}

	@Override
	public String deposit(String userId, String amount) {
		JSONObject json = new JSONObject();
		json.put("isDepositSuccessful", true);
		json.put("currentBalance", "10");
		return json.toString();
	}

	@Override
	public String withdrew(String userId, String amount) {
		JSONObject json = new JSONObject();
		json.put("isWithdrawSuccessful", true);
		json.put("currentBalance", "5");
		return json.toString();
	}

	@Override
	public String transfer(String userId, String amount, String anotherUserId) {
		JSONObject json = new JSONObject();
		json.put("isTransferSuccessful", true);
		json.put("currentBalance", "2");
		return json.toString();
	}

	@Override
	public String getTransactions(String userId) {
		JSONObject json = new JSONObject();
		JSONObject transactions = new JSONObject();
		json.put("transactions", transactions);
		return json.toString();
	}

	@Override
	public String getBalance(String userId) {
		JSONObject json = new JSONObject();
		json.put("userId", "100Z");
		json.put("balance", "1000");		
		return json.toString();
	}

}
