package com.intevalue.minibank.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intevalue.minibank.service.BaseService;
import com.intevalue.minibank.user.UserAction;
import com.intevalue.minibank.user.details.TransactionDetails;

@Service
public class Service1 extends BaseService {
	
	@Autowired
	private UserAction userAction;
	
	@Autowired
	private TransactionDetails transactionDetails;

	@Override
	public String getUserCount() {
		return userAction.getUserCount();
	}

	@Override
	public String deposit(String userId, String amount) {
		return userAction.deposit(userId, amount);
	}

	@Override
	public String withdrew(String userId, String amount) {
		return userAction.withdrew(userId, amount);
	}

	@Override
	public String transfer(String userId, String amount, String anotherUserId) {
		return userAction.transfer(userId, amount, anotherUserId);
	}

	@Override
	public String getTransactions(String userId) {
		return transactionDetails.getTransactions(userId);
	}

	@Override
	public String getBalance(String userId) {
		return userAction.getBalance(userId);
	}

	
}
