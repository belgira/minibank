package com.intevalue.minibank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.intevalue.minibank.service.BaseService;

@RestController
public class MiniBankController {
	
	@Autowired
	private ApplicationContext context;
	
	private static final String SERVICE = "service";
	
	@GetMapping(value = "/user-count", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getUserCount(@RequestParam String version) {
		BaseService service = (BaseService) context.getBean(SERVICE + version);		
		String result = service.getUserCount();
		return ResponseEntity.ok(result);      
    }
	
	@PostMapping(value = "/deposit", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deposit(@RequestParam String version, @RequestHeader String userId, @RequestHeader String amount) {
		BaseService service = (BaseService) context.getBean(SERVICE + version);		
		String result = service.deposit(userId, amount);
		return ResponseEntity.ok(result);      
    }
	
	@PostMapping(value = "/withdrew", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> withdrew(@RequestParam String version, @RequestHeader String userId, @RequestHeader String amount) {
		BaseService service = (BaseService) context.getBean(SERVICE + version);		
		String result = service.withdrew(userId, amount);
		return ResponseEntity.ok(result);      
    }
	
	@PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> transfer(@RequestParam String version, @RequestHeader String userId, @RequestHeader String amount, @RequestHeader String anotherUserId) {
		BaseService service = (BaseService) context.getBean(SERVICE + version);		
		String result = service.transfer(userId, amount, anotherUserId);
		return ResponseEntity.ok(result);      
    }
	
	@PostMapping(value = "/get-transactions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getTransactions(@RequestParam String version, @RequestHeader String userId) {
		BaseService service = (BaseService) context.getBean(SERVICE + version);		
		String result = service.getTransactions(userId);
		return ResponseEntity.ok(result);      
    }
	
	@PostMapping(value = "/get-balance", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getBalance(@RequestParam String version, @RequestHeader String userId) {
		BaseService service = (BaseService) context.getBean(SERVICE + version);		
		String result = service.getBalance(userId);
		return ResponseEntity.ok(result);      
    }

}
